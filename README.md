## Cryptanalysis 2017/2018

### Table of contents
1. [List of files](#list_of_files)
2. [How to use](#how_to_use)


### List of files:
- ***s_blocks.txt*** - contains in first line pattern all s-boxes, next lines are data to s-box, one line per one s-box
- ***DifferentialAnalyzer.py*** - analyze each s-box, create differential table and try find probably key for each s-box
- ***encrypt_image.py*** - encrypts all images in specified directory which choosen modes and show on one plot
- ***report/**** - folder contains pdf file with report

### How to use

Local machine:
1. Clone repository 
  ``` 
  git clone https://gitlab.com/PUT17/kryptoanaliza.git 
  ```
2. Install the required libraries using e.g. 
  ```
  pip install -r .\requirements.txt 
  ```
3. Enter to main directory and run script in terminal e.g
  ``` 
  python DifferentialAnalyzer.py
  ```  

Docker:
1. Install [Docker](https://www.docker.com/)
2. Clone repository 
    ```
    git clone https://gitlab.com/PUT17/kryptoanaliza.git
    ```
3. Enter to main folder and run 
    ```
    docker-compose up -d --build
    ```
4. Check name of created container by run (should be somethink like ```kryptoanaliza_cryptanalysis_1```)
    ```
    docker ps 
    ```
5. To enter into container type:
    ```
    docker exec -t -i kryptoanaliza_cryptanalysis_1 bash
    ```
6. Enter to main directory and run script in terminal e.g
    ``` 
    python DifferentialAnalyzer.py
    ```  