
from Crypto.Cipher import AES
from Crypto import Random

import matplotlib.pyplot as plt
import matplotlib.image as img
import numpy as np
import os
import argparse
from copy import deepcopy
import random
import matplotlib
from PIL import Image


def PAD(s): return s + (32 - len(s) % 32) * ' '


def ecb_plus(image_array, key, plus=True):
    if plus == True:
        for index, _ in np.ndenumerate(image_array):
            image_array[index] = (
                image_array[index] +
                # key[sum(index) % len(key)] +
                # key[int(np.mean(index)) % len(key)]
                (index[0] * key[sum(index) % len(key)]) * sum(index) +
                (index[1] * key[int(np.mean(index)) % len(key)]) * int(np.mean(index)) +
                (index[2] * key[np.sum(index) %
                                len(key)] * int(np.mean(index)))
            ) % 256
        return image_array.tostring()
    else:
        for index, _ in np.ndenumerate(image_array):
            image_array[index] = (
                image_array[index] -
                # key[sum(index) % len(key)] +
                # key[int(np.mean(index)) % len(key)]
                (index[0] * key[sum(index) % len(key)]) * sum(index) -
                (index[1] * key[int(np.mean(index)) % len(key)]) * int(np.mean(index)) -
                (index[2] * key[np.sum(index) %
                                len(key)] * int(np.mean(index)))
            ) % 256
        return image_array


class EncryptionImage:
    """
    Class to encrypt image in specified mode.
    Possible modes: cbc, ecb only, for other images will be process as cbc
    """

    def __init__(self, input_image, key=0):
        """
        Initialize necessary variable

        :param input_image: path to image that should be process
        """
        self.input_image = input_image
        self.MODES = {'cbc': AES.MODE_CBC,
                      'ecb': AES.MODE_ECB,
                      'ecb+': AES.MODE_ECB
                      }
        self.image_shape = 0
        self.original_image = 0
        self.noise = 0
        self.key = key
        self.processed_image = 0
        self.iv = Random.new().read(AES.block_size)
        try:
            self.original_image = img.imread(
                self.input_image)
            self.img_shape = self.original_image.shape
        except FileNotFoundError as e:
            print(f'Error: {e}')
        except Exception as ex:
            print(f'Error: {ex}')

    def encrypt(self, mode='cbc'):
        """
        Encrypts image in choosen mode and return image.

        :param mode: available mode to encryption
        :return enc_image: encrpted image as numpy array
        """
        image_array = np.asarray(self.original_image, dtype=int)

        if not self.key:
            self.key = os.urandom(16)

        if mode in self.MODES.keys():
            if mode == 'ecb+':
                image_string = ecb_plus(deepcopy(image_array), self.key)
            else:
                image_string = image_array.tostring()
            mode = self.MODES[mode]
        else:
            mode = AES.MODE_CBC

        if mode == AES.MODE_ECB:
            cipher_image = AES.new(self.key, mode)
        else:
            cipher_image = AES.new(self.key, mode, self.iv)

        msg_enc = cipher_image.encrypt(image_string)

        enc_image = np.fromstring(msg_enc, dtype='<f4')
        # enc_image = np.asarray(enc_image, dtype=np.float32)
        enc_image = enc_image.reshape(self.img_shape)
        self.processed_image = enc_image

        return self.processed_image

    def decrypt(self, mode='ecb'):
        bool_ecb_plus = False
        if mode in self.MODES.keys():
            if mode == 'ecb+':
                bool_ecb_plus = True
            mode = self.MODES[mode]
        else:
            mode = AES.MODE_CBC

        # np.asarray(self.processed_image * 256, dtype=int)
        image_array = self.processed_image

        image_string = image_array.tostring()

        if mode == AES.MODE_ECB:
            encrypted_image = AES.new(self.key, mode)
        else:
            encrypted_image = AES.new(self.key, mode, self.iv)

        msg_decrypted = encrypted_image.decrypt(image_string)

        decrypted_image = np.fromstring(msg_decrypted, dtype=np.uint32)

        # decrypted_image = np.asarray(decrypted_image, dtype=np.float32)
        decrypted_image = decrypted_image.reshape(self.img_shape)

        if bool_ecb_plus:
            self.processed_image = ecb_plus(
                deepcopy(decrypted_image), self.key, False)
        else:
            self.processed_image = decrypted_image
        return self.processed_image


def show_images(images, cols=1, titles=None):
    """
    Display a list of images in a single figure with matplotlib.

    :param images: list of np.arrays compatible with plt.imshow.
    :param cols: number of columns in figure (number of rows is set to round(n_images/float(cols)), default = 1
    :param titles: list of titles to every images
    """

    assert((titles is None)or (len(images) == len(titles)))
    n_images = len(images)
    if titles is None:
        titles = ['Image (%d)' % i for i in range(1, n_images + 1)]
    fig = plt.figure()
    for n, (image, title) in enumerate(zip(images, titles)):
        a = fig.add_subplot(cols, round(n_images/float(cols)), n + 1)
        if image.ndim == 2:
            plt.gray()
        plt.imshow(image)
        a.set_title(title)
    fig.set_size_inches(np.array(fig.get_size_inches()) * n_images)
    plt.show()


def encrypt_more_images(path_to_images='./images', modes=['ecb'], output_path='./encrypted_images'):
    """
    Fuction get all image from path_to_images directory, encrypts in every mode specified in modes variable and show result on plot

    :param path_to_images: path to root directory contents images to process, default='./images'
    :param modes: list with allowed encryption mode, default=['cbc',]
    :param output_path: path to directory where will be save encrypted images
    """
    my_images = list()
    files_set = set()
    my_titles = list()
    key = b'robertkowalskist'

    number_of_files = 0
    for root, _, files in os.walk(path_to_images):
        number_of_files = len(files)
        for fileName in files:
            files_set.add(os.path.join(root, fileName))

    encryption_images = list()
    if not os.path.exists(output_path):
        os.mkdir(output_path)

    dec = 0
    for image_path in files_set:
        my_encryption = EncryptionImage(image_path, key=key)
        my_images.append(my_encryption.original_image)
        file_name = os.path.basename(image_path)
        my_titles.append(f'original {file_name}')

        for mode in modes:
            image = my_encryption.encrypt(mode=mode)
            dec = my_encryption.decrypt(mode=mode)
            # pil_image = Image.fromarray(image.astype('uint8'), 'RGB')
            # pil_image.save(os.path.join(output_path, f'{mode}_{file_name}'))
            my_images.append(image)
            my_images.append(dec)
            my_titles.append(f'{mode} {file_name}')
            my_titles.append(f'dec {mode} {file_name}')
        encryption_images.append(my_encryption)

    show_images(images=my_images,
                titles=my_titles,
                cols=number_of_files
                )


if __name__ == '__main__':
    # Root directory with images
    path_to_data = './images'
    # Selected modes for encryption
    modes = ['ecb', 'ecb+']

    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--path', help='path to images, defaul = "./images"', type=str)
    parser.add_argument(
        '--modes', help='list with modes to process, e.g "ecb,cbc"  default="cbc,"', type=str)

    args, unparsed = parser.parse_known_args()

    if args.path:
        path_to_data = args.path
    if args.modes:
        modes = str(args.modes)
        modes.replace(" ", "")
        modes = modes.split(',')

    # Encrypt and show all image in root
    encrypt_more_images(path_to_images=path_to_data, modes=modes)
    # ,modes=modes)
