import itertools as it
import numpy as np
from statistics import median
from copy import deepcopy
import random
import heapq


class SBox:
    """
    Class use for storage all s box data
    """

    def __init__(self, data):
        self.data = data
        self.key = random.randint(0, 63)  # key
        self.pattern = 0
        # self.xor_pair = dict()
        self.chosen_indexes = list()
        self.difference_table = 0
        self.pair_table_given_value = dict()
        self.dict_of_probably_key = dict()

    def get_most_n_probably_keys(self, number=1):
        most_probably = {key: value for key, value in self.dict_of_probably_key.items(
        ) if value in sorted(set(self.dict_of_probably_key.values()), reverse=True)[:number]}
        return most_probably

    def pushed_through_s_box(self, input):
        index = self.pattern.index(int(input) ^ int(self.key))
        return self.data[index]


class DifferentialAnalyzer:
    """
    Create differential tables for each s-blocks, First must load data with s-blocks.
    """

    def __init__(self):
        """
        Reset all variable used in class.
        """
        self.s_blocks = list()
        self.pattern = 0
        self.xor_pair_pattern = dict()
        self.differences_tables = list()
        self.xor_pair_s_boxes = list()
        self.chosen_indexes_each_s_box = list()

    def load_s_blocks(self, file_path='s_blocks'):
        """
        Load data with s-blocks from file.
        File structures must be like:
        '''
            0 2 4 6 8 10 12 14 16 18  fist line is pattern data
            14 4 13 1 2 15 11 8 3 10 6 12 5 9 0 7 0 15 every next one row is s-block data
        '''
        :param file_path:
        :return:
        """
        try:
            with open(file_path, 'r') as reader:
                pattern = reader.readline().split(' ')
                self.pattern = list(map(int, pattern))
                for row in reader:
                    data = row.split(' ')
                    data = list(map(int, data))
                    self.s_blocks.append(SBox(data))

        except FileExistsError as e:
            print("I/O error({0}): {1}".format(e.errno, e.strerror))
        except FileNotFoundError as e:
            print("I/O error({0}): {1}".format(e.errno, e.strerror))

    def get_xor_pair_pattern(self):
        """
        Calculate xor value for each permutations with size two from pattern. Returning dictionary where key is
        tuple consisting of number index elements which was created. Value is calculation of xor this elements.
        Example:
        pattern: 1 2 3
        permutations: [1, 2], [1, 3], [2, 3]
        calculation: pattern[0] xor pattern[1] -> {(0, 1): 3}
                     pattern[0] xor pattern[2] -> {(0, 2): 2}
                     pattern[1] xor pattern[2] -> {(1, 2): 1}
        :return:
        """
        if not self.pattern:
            print('Do not load data.')
            return 1
        if self.xor_pair_pattern:
            self.xor_pair_pattern = dict()
        for pair in list(it.permutations(range(len(self.pattern)), 2)):
            self.xor_pair_pattern[(
                pair[0], pair[1])] = self.pattern[pair[0]] ^ self.pattern[pair[1]]
        return self.xor_pair_pattern

    def create_differential_tables(self):
        """
        Create for each s-block table of differential. Data stored in each Sbox.difference_table
        Craete for each s-block table of pair, each cell contain list of pairs, e.g
        '''''''''''''''''''''''''''''''''''''''''''''''''
         differential table               pair table
           0 1                              0 1
         0                                0
         1   2                            1  [12, 23, 11]

        means - for xor pair on out = 1 and xor pair on in = 1 create value 2 in
        diferential table and stored this pair [12, 23, 11] in pair table without repeats
        ''''''''''''''''''''''''''''''''''''''''''''''''''
        :return:
        """
        if not self.xor_pair_pattern:
            self.get_xor_pair_pattern()

        for one_s_block in self.s_blocks:
            xor_pair_s_box = dict()

            for pair in list(it.permutations(range(len(self.pattern)), 2)):
                xor_value = one_s_block.data[pair[0]
                                             ] ^ one_s_block.data[pair[1]]
                xor_pair_s_box[(pair[0], pair[1])] = xor_value

            # self.xor_pair_s_boxes.append(dict_of_s_box_values_with_gaves_xor)

            difference_table = np.zeros((len(self.pattern), 16)).astype(int)
            # Magic :D create empty table filled each cell empty list :D
            pair_table = np.empty((len(self.pattern), 16), dtype=object)
            pair_table.fill([])
            pair_table = np.frompyfunc(list, 1, 1)(pair_table)

            for pair in list(it.permutations(range(len(self.pattern)), 2)):
                difference_table[self.xor_pair_pattern[pair]
                                 ][xor_pair_s_box[pair]] += 1
                indexes = translator(self.pattern, (pair[0], pair[1]))
                pair_table[self.xor_pair_pattern[pair]
                           ][xor_pair_s_box[pair]] += list(indexes)
                pair_table[self.xor_pair_pattern[pair]][xor_pair_s_box[pair]] = list(
                    set(pair_table[self.xor_pair_pattern[pair]][xor_pair_s_box[pair]]))

            difference_table[0][0] = 64
            one_s_block.difference_table = deepcopy(difference_table)
            one_s_block.pair_table_given_value = deepcopy(pair_table)
            one_s_block.pattern = deepcopy(self.pattern)

    def get_differences_tables(self):
        """
        Return list of diferential table for each s_box
        :return list:
        """
        list_of_diference_tables = list()
        for s_box in self.s_blocks:
            list_of_diference_tables.append(s_box.difference_table)
        return list_of_diference_tables

    def calculate_best_row_in_each_s_block(self, number_of_rows=3):
        """
        On base max and mediana value of each row in each s block chose best numbers of rows and store in each s-block
        :param number_of_rows int default 1:
        :return:
        """
        for number_of_s_block in range(len(self.s_blocks)):
            dict_of_rows_max_sum = {ind: (max(val), median(val))
                                    for ind, val in list(enumerate(self.s_blocks[number_of_s_block].difference_table))}
            list_of_index = sorted(dict_of_rows_max_sum.keys(
            ), key=lambda x: dict_of_rows_max_sum[x], reverse=True)
            self.s_blocks[number_of_s_block].chosen_indexes = list_of_index[1:int(
                number_of_rows+1)]

    def get_dict_of_table_best_rows(self):
        """
        Return dictionary of pairs for each s-box and best rows in this box.
        :return dictionary:
        """
        tables_dict = dict()
        for element in range(len(self.s_blocks)):
            dict_of_pair_chosen_index = dict()
            for row in self.s_blocks[element].chosen_indexes:
                dict_of_pair_chosen_index[row] = self.s_blocks[element].pair_table_given_value[row]
            tables_dict[element] = dict_of_pair_chosen_index
        return tables_dict

    def get_table_of_pair(self, sbox_number, row_number):
        """
        Get table of pair for s box number n and row number m.
        :param sbox_number:
        :param row_number:
        :return list:
        """
        return self.s_blocks[sbox_number].pair_table_given_value[row_number]

    def search_key(self, best_cell=2):
        """
        Try find key for each s-boxes 
        :param best_cell: use specified number of cell with hightest number in differential table
        """
        for s_block in self.s_blocks:
            for row in s_block.chosen_indexes:
                for inputs in [(x, y) for x in range(0, 63) for y in range(0, 63) if int(x) ^ int(y) == row]:
                    input_1 = inputs[0]
                    input_2 = inputs[1]

                    s_box_output_1 = s_block.pushed_through_s_box(input_1)
                    s_box_output_2 = s_block.pushed_through_s_box(input_2)

                    xor_outputs = s_box_output_1 ^ s_box_output_2

                    # self.data[my_input ^ self.key]
                    max_index_in_row = list(map(s_block.difference_table[row].tolist().index, heapq.nlargest(
                        2, s_block.difference_table[row].tolist())))

                    if xor_outputs not in max_index_in_row:
                        continue

                    for max_index in max_index_in_row:
                        for element in s_block.pair_table_given_value[row][max_index]:
                            # for element in cell:
                            xor_value = input_1 ^ element
                            # xor_value = self.data[my_input ^
                            #   self.key] ^ element  # ^ my_input

                            if xor_value in s_block.dict_of_probably_key:
                                s_block.dict_of_probably_key[xor_value] += 1
                            else:
                                s_block.dict_of_probably_key[xor_value] = 1

    def display_probably_key(self,):
        for number in range(len(self.s_blocks)):
            print(f"Probably key in s-block number {number}:")
            tmp = self.s_blocks[number].get_most_n_probably_keys()
            for result in tmp.items():
                print(f"Key {result[0]} occurred {result[1]} times")
            print("Set key at the beginning:")
            print(self.s_blocks[number].key)
            print()


def get_random_int():
    return int(bin(random.getrandbits(6)))


def translator(s_box, index):
    """
    Translate number of indexes into s-box value or pattern value
    :param s_box:
    :param index tuple:
    :return tuple:
    """
    return (int(s_box[index[0]]), int(s_box[index[1]]))


if __name__ == "__main__":
    # Initialize object
    my_s_box_analyzer = DifferentialAnalyzer()
    # Loading data from file
    my_s_box_analyzer.load_s_blocks()
    # Create diefferential tables for each loaded s boxes
    my_s_box_analyzer.create_differential_tables()
    # Finding best row wor future processing
    my_s_box_analyzer.calculate_best_row_in_each_s_block()
    # Trying find key for each s box
    my_s_box_analyzer.search_key()
    # Display result
    my_s_box_analyzer.display_probably_key()
